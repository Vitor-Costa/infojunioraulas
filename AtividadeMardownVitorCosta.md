 # MARKDOWN - Guia Prático
 
 ## Conceito:

Segundo o site [PIPZ ACADEMY](https://docs.pipz.com/central-de-ajuda/learning-center/guia-basico-de-markdown#),  Markdown Syntax é uma sintaxe usada para padronizar e facilitar formatação de texto na web, utilizada em aplicativos como Slack e GitHub. Textos estilizados com **Markdown** são, na maioria dos casos, apenas texto com caracteres não-alfabéticos, como #, \\* e \! \[] \(), usados para a configuração de títulos, listas, itálico, negrito e inserção de imagens.

### Lista de comandos

#### Titulos:

Para criar titulos devemos utilizar o sinal de cerquilha # ao incio do titulo, comparando a um padrão html,  a utilização de #  serria comparado ao h1, ## comparado ao h2 e assim por diante:

```
# Título <h1>
## Título <h2>
### Título <h3>
#### Título <h4>
##### Título <h5>
###### Título <h6>
```
##### Vizualização dos codigos acima:

# Título 
## Título 
### Título 
#### Título
##### Título 
###### Título 

---

#### Ênfase:

Para adicionar ênfase ao conteúdo que será escrito, usa-se o asterisco * ou traço-baixo (underline) _:

* **Negrito**: deve-se adicionar dois ateriscos ou dois traços-baixos no inicio e no fim do conteudo.
    ###### Exemplos: 
    \*\*texto\*\* , \_\_texto\_\_ 

    ###### Vizualização: 
    **texto**,   __texto__


  &nbsp;

* **Itálico**:  deve-se adicionar um aterisco ou um traço-baixo no inicio e no fim do conteudo.
    ###### Exemplos: 
    \*texto\* , \_texto\_ .

    ###### Vizualização:
    *texto* , _texto_ .

---

#### Links:

No Markdowns é possivel inserir links de duas formas diferentes: através de um **link direto** ou usando um **texto-âncora**:

* **Texto âncora**: para utilizar um texto como âncora d eum link deve-se utilizar os colchetes [] para inserir qual texto você deseja que apareça, e os parenteses () para inserir o link para a página desejada. Exemplo: ```[Google](https://google.com/) ```. A exibição para este comando será a seguinte:  [Google](https://google.com/)
  
   &nbsp;

* **Link direto**: deve-se envolver o endereço web em chaves <>. O mesmo ficará visivel e clicável pelo o usuário. Exemplo: ``` <https://www.google.com/> ```. A exibição para este comando será a seguinte: <https://www.google.com/>
  

---

#### Listas de itens

##### Listas não Ordenadas
 Para listas de itens não ordenadas deve-se utilizar um asterisco na frente do item da lista:

###### Exemplo:
```
* Item 1
* Item 2
* Item 3
```
###### Vizualização:

* Item 1
* Item 2
* Item 3

##### Listas Ordenadas
 Para listas de itens não ordenadas deve-se utilizar o número do item seguido de ponto .

###### Exemplo:
 ```
1. Item 1
2. Item 2
3. Item 3
 ```
###### Vizualização:

1. Item 1
2. Item 2
3. Item 3

---

#### Lista de tarefas

As listas de tarefas permitem criar uma lista de itens com caixas de seleção. Para criar uma lista de tarefas, adicione traços  "-"  e colchetes com um espaço "[ ]" na frente dos itens da lista de tarefas. Para pré-selecionar uma caixa de seleção, adicione um x entre colchetes ( [x]).

###### Exemplo:

```
- [x] Arrumar a cama
- [ ] Varrer a casa
- [ ] Lavar os pratos
```

###### Vizualização:

- [x] Arrumar a cama
- [ ] Varrer a casa
- [ ] Lavar os pratos


---

#### Tabelas

Para adicionar uma tabela, use três ou mais hífens ( ---) para criar o cabeçalho de cada coluna e use pipes ( |) para separar cada coluna. Como opção, você pode adicionar tubos nas duas extremidades da tabela.

###### Exemplo:
```
| Produto     |  Código |    Valor     |
| ----------- | --------| ------------ |
| Notebook    |   001   | R$ 2000,00   |
| Tablet      |   002   | R$ 1000,00   |
```

###### Vizualização:

| Produto     |  Código  |    Valor     |
| ----------- | -------- | ------------ |
| Notebook    |   001    | R$ 2000,00   |
| Tablet      |   002    | R$ 1000,00   |

---

#### Blocos de código

Há dois modos de adicionar trechos de código ao Markdown:

Código em linha (inline): adicione um acento grave ˋ no início e no final do código.
Múltiplas linhas de código: envolva as linhas de código com três acentos graves ˋˋˋ ou três tils ~~~.

###### Exemplos:

     Esta é uma linha que contém um `código`.

  
    ```
    Esta é uma linha de código.
    ```


    ~~~
    Esta é uma linha de código.
    ~~~


###### Vizualização:


Esta é uma linha que contém um `código`.

  
```
Esta é uma linha de código.
```


~~~
Esta é uma linha de código.
~~~

---
#### Considerações

Além destes comandos existem vários outros que estão disponiveis para a formatação de documentos em formato md. Para mais informações acesse o
[ The Markdown Guide ](https://www.markdownguide.org/).

